const { SERVER_ERROR, GROUP_STATUS, STATUS } = require("../configs/config");
const Group = require("../models/Group.model");
const User = require("../models/User.model");


//Tạo một nhóm mới dựa trên thông tin người dùng cung cấp, bao gồm tên, mô tả và trạng thái.
const CreateGroupService = async (id, data) => {
  try {
    const user = await User.findById(id);
    if (user) {
      const group = new Group({
        admin: user._id,
        name: data.name,
        description: data?.description,
        status:
          data?.status === "public"
            ? GROUP_STATUS.PUBLIC
            : GROUP_STATUS.PRIVATE,
      });

      if (group) {
        await group.save();
        return {
          result: "success",
          status: STATUS.CREATE_OK,
          group: group,
        };
      }

      return {
        result: "failed",
        status: STATUS.INTERNAL_SERVER_ERROR,
        reason: "Lỗi máy chủ",
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

//Tìm kiếm nhóm dựa trên từ khóa và trả về danh sách các nhóm phù hợp.
const SearchGroupService = async (data) => {
  try {
    const limit = data.limit ? data.limit : 10;
    const page = data.page ? data.page : 1;
    const group = await Group.find(
      {
        $or: [
          {
            name: { $regex: `${data.q}` },
          },
          {
            description: { $regex: `${data.q ? data.q : ''}` },
          },
        ],
      },
      {},
      { limit: limit, skip: limit * (page - 1) }
    );

    return {
      result: "success",
      status: 200,
      group: group,
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

//Hàm này kiểm tra xem một người dùng có phải là quản trị viên của một nhóm hay không
const isAdminOfGroup = async (groupId, userId) => {
  try {
    const isAdmin = await Group.findOne(
      { $and: [{ _id: groupId }, { admin: userId }] },
      (err, doc) => {
        if (err) {
          console.log(err);
          return false;
        }

        if (doc) {
          console.log(doc);
          return true;
        } else {
          console.log("not found gourp of you");
          return false;
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

//Xóa một nhóm khỏi hệ thống nếu người dùng có quyền thực hiện hoạt động này.
const DeleteGroupService = async ({ groupId, userId, role }) => {
  try {
    const group = await Group.findById(groupId);
    if (group) {
      console.log(group.admin);
      if (role === 0 || (role === 1 && group.admin == userId)) {
        await group.deleteOne();
        return {
          result: "success",
          status: STATUS.OK,
        };
      }
      return {
        result: "failed",
        status: STATUS.FORBIDDEN,
        reason: "Không được cấp quyền!",
      };
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy nhóm này",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

//Thay đổi trạng thái của một nhóm từ công khai thành riêng tư hoặc ngược lại, dựa trên quyền của người dùng.
const ChangeStatusGroupService = async ({ groupId, userId, role, status }) => {
  try {
    const group = await Group.findById(groupId);
    if (group) {
      if (role === 0 || (role === 1 && group.admin == userId)) {
        await group.set({
          status:
            status === GROUP_STATUS.PUBLIC
              ? GROUP_STATUS.PUBLIC
              : GROUP_STATUS.PRIVATE,
        });

        await group.save();
        return {
          result: "success",
          status: STATUS.OK,
          group: group,
        };
      }
      return {
        result: "failed",
        status: STATUS.FORBIDDEN,
        reason: "Không được cấp quyền!",
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy nhóm này",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

//Hàm này loại bỏ một thành viên khỏi một nhóm
const RemoveMemberFromGroup = async ({ groupId, userId, memberId, role }) => {
  try {
    const group = await Group.findById(groupId);
    if (group) {
      if (role === 0 || (role === 1 && group.admin == userId)) {
        const listMember = group.member;
        const member = listMember.find((id) => id === userId);
      }
      return {
        result: "failed",
        status: STATUS.FORBIDDEN,
        reason: "Không được cấp quyền!",
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy nhóm này",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

//Đổi tên một nhóm nếu người dùng có quyền thực hiện thao tác này.
const RenameGroupService = async ({ groupId, userId, role, name }) => {
  try {
    const group = await Group.findById(groupId);
    if (group) {
      if (role === 0 || (role === 1 && group.admin == userId)) {
        await group.set({
          name: name,
        });
        await group.save();
        return {
          result: "succes",
          status: STATUS.OK,
          group: group,
        };
      }
      return {
        result: "failed",
        status: STATUS.FORBIDDEN,
        reason: "Không được cấp quyền!",
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy nhóm này",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

//Cho phép người dùng tham gia vào một nhóm cụ thể.
const JoinToGroupService = async ({ groupId, memberId }) => {
  try {
    const group = await Group.findById(groupId);
    if (group) {
      let memberRequest = group.memberRequest;
      let member = group.member;
      if (group.status === GROUP_STATUS.PRIVATE) {
        if (memberRequest.find((id) => id === memberId))
          return {
            result: "success",
            status: STATUS.OK,
            message: "Please dont send request",
          };
        memberRequest.push(memberId);
        await group.set({
          memberRequest: memberRequest,
        });
        await group.save();
        return {
          result: "success",
          status: STATUS.OK,
          group: group,
        };
      }
      if (group.status === GROUP_STATUS.PUBLIC) {
        if (member.find((id) => id === memberId))
          return {
            result: "success",
            status: STATUS.OK,
            message: "Please dont send request",
          };
        member.push(memberId);
        await group.set({
          member: member,
        });
        await group.save();
        return {
          result: "success",
          status: STATUS.OK,
          group: group,
        };
      }
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy nhóm này",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

// Cho phép người dùng rời khỏi một nhóm mà họ đã tham gia trước đó.
const LeaveFromGroupService = async ({ groupId, userId }) => {
  try {
    const group = await Group.findById(groupId);
    if (group) {
      let memberRequest = group.memberRequest;
      let member = group.member;
      if (group.status === GROUP_STATUS.PRIVATE) {
        const indexLeave = memberRequest.findIndex((id) => id === userId);
        if (indexLeave) {
          memberRequest.splice(indexLeave, 1);
          await group.set({
            memberRequest: memberRequest,
          });
          await group.save();
          return {
            result: "success",
            status: STATUS.OK,
            group: group,
          };
        }
      }

      if (group.status === GROUP_STATUS.PUBLIC) {
        const indexLeave = member.findIndex((id) => id === userId);
        if (indexLeave) {
          member.splice(indexLeave, 1);
          await group.set({
            member: member,
          });
          await group.save();
          return {
            result: "success",
            status: STATUS.OK,
            group: group,
          };
        }
      }

      return {
        result: "failed",
        status: STATUS.NOT_FOUND,
        reason: "Không tìm thấy người dùng trong nhóm",
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy nhóm này",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

// Hàm truy vấn cơ sở dữ liệu để lấy danh sách thành viên của một nhóm
const GetGroupMembers = async (groupId) => {
  try {
    const group = await Group.findById(groupId).populate('member', 'username');
    if (group) {
      return {
        result: 'success',
        status: STATUS.OK,
        members: group.member,
      };
    }
    return {
      result: 'failed',
      status: STATUS.NOT_FOUND,
      reason: 'Không tìm thấy nhóm này',
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

// Hàm truy vấn cơ sở dữ liệu để lấy danh sách các nhóm mà một người dùng đã tham gia
const GetUserGroups = async (userId) => {
  try {
    const groups = await Group.find({ member: userId });
    return {
      result: 'success',
      status: STATUS.OK,
      groups: groups,
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

module.exports = {
  CreateGroupService,
  DeleteGroupService,
  ChangeStatusGroupService,
  RenameGroupService,
  JoinToGroupService,
  LeaveFromGroupService,
  SearchGroupService,
  GetGroupMembers,
  GetUserGroups,

};
