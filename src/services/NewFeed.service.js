const { set, ObjectId } = require("mongoose");
const { STATUS } = require("../configs/config");
const { STATUS_POST, NUM_SUGGESTION } = require("../configs/constant");
const Post = require("../models/Post.model");
const User = require("../models/User.model");
const _ = require("lodash");

const GetPublicNewFeedService = async (id) => {
  try {
    const arrPost = await Post.find({
      status: STATUS_POST.PUBLIC,
    });

    const arrPostFl = await Post.find({
      status: STATUS_POST.FOLLOWER,
    });

    const user = await User.findById(id);

    console.log("abc");
    arrPostFl.map((post) => {
      const index = user.followings.includes(post.userId);
      console.log("index: ", index);
      if (index !== -1) {
        arrPost.push(post);
      }
    });

    await arrPost.sort((a, b) => {
      if (a.updatedAt > b.updatedAt) return -1;
      else if (a.updatedAt > b.updatedAt) return 1;
      else return 0;
    });
    // const post = []
    // const user = await User.findById(id)
    // await Promise.all(
    //   user.followings.map( async ID => {
    //     const postFl = await Post.find({
    //       userId: new ObjectId(ID)
    //     })
    //     post.push(postFl)
    //   })
    // )

    return {
      result: "success",
      status: STATUS.OK,
      post: arrPost,
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetSuggestionService = async (userId) => {
  try {
    const user = await User.find();
    let arrFling = [];
    const index = user.findIndex(u => u._id.toString() == userId)
    user.splice(index, 1)
    user.map((_user) => {
      if(!_user.followers.some(u => u.id.toString() == userId)) {
        arrFling.push(_user)
      }
    });
    return {
      result: "success",
      status: STATUS.OK,
      user: arrFling,
      // user: _.sampleSize(listUserSuccesstion, NUM_SUGGESTION),
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

module.exports = { GetPublicNewFeedService, GetSuggestionService };
