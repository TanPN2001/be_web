const bcrypt = require("bcrypt");
const {
  SERVER_DB_ERROR,
  CONFIG,
  SERVER_ERROR,
  STATUS,
  TOKENTYPE,
} = require("../configs/config");
const { signToken } = require("../middlewares/jwt");
const User = require("../models/User.model");
const { uploadToCloudinary } = require("./cloudinary");
const { createToken } = require("./Token.service");
const { DEFAULT_USER } = require("../configs/constant");

const RegisterUserService = async (data) => {
  try {
    const findUser = await User.findOne({ email: data.email });

    if (findUser) {
      return {
        result: "failed",
        reason: "Email này đã đăng kí tài khoản",
        status: 400,
      };
    }
    const hash = bcrypt.hashSync(data.password, Number(CONFIG.bcrypt_salt));
    const user = new User({
      role: 1,
      email: data?.email,
      password: hash,
      fullname: data?.fullname,
      phone: data?.phone,
      username: data?.username,
      status: 1,
      avatar: DEFAULT_USER.AVATAR,
    });

    if (user) {
      await user.save();
      return {
        result: "success",
        status: 201,
        user: {
          email: user.email,
          fullname: user.fullname,
          phone: user.phone,
          username: user.username,
        },
      };
    }

    return SERVER_DB_ERROR;
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const LoginUserService = async (data) => {
  try {
    const findUser = await User.findOne({
      email: data.email,
    });
    if (findUser) {
      if (findUser.status !== 1)
        return {
          result: "failed",
          reason: "Tài khoản đã bị khóa",
          status: 401,
        };

      if (bcrypt.compareSync(data.password, findUser.password)) {
        const accessToken = await createToken({
          id: findUser._id,
          type: TOKENTYPE.refreshToken,
          role: findUser.role,
          status: findUser.status,
          expire: CONFIG.jwt_expire_access_token,
        });

        const refreshToken = await createToken({
          id: findUser._id,
          type: TOKENTYPE.refreshToken,
          role: findUser.role,
          status: findUser.status,
          expire: CONFIG.jwt_expire_refresh_token,
        });

        await findUser.updateOne({
          $set: {
            accessToken: accessToken,
            refreshToken: refreshToken,
          },
        });
        await findUser.save();

        return {
          result: "success",
          status: 200,
          accessToken: accessToken,
          refreshToken: refreshToken,
        };
      }
      return {
        result: "failed",
        status: STATUS.BAD_REQUEST,
        reason: "Sai mật khẩu",
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const LogoutService = async (id) => {
  try {
    const user = await User.findById(id);
    if (user) {
      await user.set({
        accessToken: null,
      });
      await user.save();

      return {
        result: "success",
      };
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const ChangePasswordService = async (data) => {
  try {
    const user = await User.findById(data.userId);
    if (user) {
      if (bcrypt.compareSync(data.oldPassword, user.password)) {
        await user.updateOne({
          password: bcrypt.hashSync(
            data.newPassword,
            Number(CONFIG.bcrypt_salt)
          ),
        });

        return {
          result: "success",
          status: STATUS.OK,
        };
      } else {
        return {
          result: "failed",
          status: STATUS.NOT_FOUND,
          reason: "Sai mật khẩu",
        };
      }
    } else {
      return {
        result: "failed",
        status: STATUS.NOT_FOUND,
        reason: "Không tìm thấy người dùng",
      };
    }
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetProfileUserService = async (id) => {
  try {
    const user = await User.findById(id);
    if (user)
      return {
        result: "success",
        status: 200,
        user: user,
      };

    return SERVER_DB_ERROR;
  } catch (error) {
    return SERVER_ERROR(error);
  }
};

const GetListUserService = async () => {
  try {
    const listUser = await User.find({}).select(["-password", "-accessToken"]);

    if (listUser)
      return {
        result: "success",
        status: 200,
        user: listUser,
      };

    return SERVER_DB_ERROR;
  } catch (error) {
    return SERVER_ERROR(error);
  }
};

const UpdateProfileService = async (data, id) => {
  try {
    const user = await User.findById({
      _id: id,
    });

    if (user) {
      user.set({
        fullname: data?.fullname,
        dob: data?.dob,
      });

      await user.save();
      return {
        result: "success",
        status: 200,
        user: user,
      };
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    return SERVER_ERROR(error);
  }
};

const UpdateAvatarService = async (file, id) => {
  try {
    const user = await User.findById({ _id: id });
    if (user) {
      const img = await uploadToCloudinary(file.path, `be-web/${id}/avatar`);
      user.set({
        avatar: img.url,
      });

      await user.save();

      return {
        result: "success",
        status: 200,
      };
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    return SERVER_ERROR(error);
  }
};

const LockUserService = async (adminId, id) => {
  try {
    const findAdmin = await User.findById({ _id: adminId });

    if (!findAdmin)
      return {
        result: "failed",
        status: STATUS.FORBIDDEN,
        reason: "Không được cấp quyền",
      };

    const findUser = await User.findById({ _id: id });
    if (!findUser) {
      return {
        result: "failed",
        status: STATUS.NOT_FOUND,
        reason: "Không tìm thấy người dùng",
      };
    }
    findUser.set({ status: 0 });
    await findUser.save();

    return {
      result: "success",
      status: 200,
      user: {
        _id: findUser.id,
        status: findUser.status,
        fullname: findUser.fullname,
      },
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const UnLockUserService = async (adminId, id) => {
  try {
    const findAdmin = await User.findById({ _id: adminId });

    if (!findAdmin)
      return {
        result: "failed",
        status: STATUS.FORBIDDEN,
        reason: "Không được cấp quyền",
      };

    const findUser = await User.findById({ _id: id });
    if (!findUser) {
      return {
        result: "failed",
        status: STATUS.NOT_FOUND,
        reason: "Không tìm thấy người dùng",
      };
    }
    findUser.set({ status: 1 });
    await findUser.save();

    return {
      result: "success",
      status: 200,
      user: {
        _id: findUser.id,
        status: findUser.status,
        fullname: findUser.fullname,
      },
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const SearchUserService = async (data) => {
  try {
    const limit = data.limit ? data.limit : 10;
    const page = data.page ? data.page : 1;
    const user = await User.find(
      {
        fullname: { $regex: `${data.q ? data.q : ""}` },
      },
      {},
      { limit: limit, skip: limit * (page - 1) }
    ).select(["-password", "-accessToken", "-role"]);

    return {
      result: "success",
      status: 200,
      user: user,
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const CreateUserService = async (data) => {
  try {
    if (data.role === 0) {
      const findUser = await User.findOne({ email: data.email });

      if (findUser) {
        return {
          result: "failed",
          reason: "Email này đã đăng kí tài khoản",
          status: 400,
        };
      }

      const hash = bcrypt.hashSync(data.password, Number(CONFIG.bcrypt_salt));
      const user = new User({
        role: 1,
        email: data?.email,
        password: hash,
        fullname: data?.fullname,
        phone: data?.phone,
        username: data?.username,
        status: 1,
      });

      if (user) {
        await user.save();
        return {
          result: "success",
          status: 201,
          user: {
            email: user.email,
            fullname: user.fullname,
            phone: user.phone,
            username: user.username,
          },
        };
      }
      return SERVER_DB_ERROR;
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy nhóm",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const findUserInArray = (arr, id) => {
  let count = 0
  arr.map(user => {
    if (user.id == id) {
      count++
    }
  })
  return count
};

const FollowService = async (followerID, userID) => {
  try {
    const findFollower = await User.findById(followerID); // người đi follower
    const findUser = await User.findById(userID); // người được follow

    if (!findFollower || !findUser) {
      return {
        result: "failed",
        status: STATUS.NOT_FOUND,
        reason: "Không tìm thấy người dùng",
      };
      console.log('1233333');
    }
    if (followerID === userID) return {
      result: 'failed',
      status: STATUS.BAD_REQUEST,
      reason: 'Bạn phải theo dõi người dùng khác'
    }

    let arrFling = findFollower.followings; // mảng những người đang following
    let arrFler = findUser.followers; // mảng những người đang được follow

    if (findUserInArray(arrFling, userID) === 0 && findUserInArray(arrFler, followerID) === 0) {
      arrFling.push({
        avatar: findUser?.avatar,
        fullname: findUser?.fullname,
        id: findUser._id,
      });
      arrFler.push({
        avatar: findFollower?.avatar,
        fullname: findFollower?.fullname,
        id: findFollower._id,
      });

      await findFollower.updateOne({
        followings: arrFling,
      });
      await findUser.updateOne({
        followers: arrFler,
      });

      console.log("findFollower: ", findFollower);
      console.log("findUser: ", findUser);
      return {
        result: "success",
        status: STATUS.OK,
      };
    }
    else {
      return {
        result: "success",
        status: STATUS.OK,
        reason: "Đã theo dõi người này",
      };
    }
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const UnFollowService = async (followerID, userID) => {
  try {
    const findFollower = await User.findById(followerID); // người đi follower
    const findUser = await User.findById(userID); // người được follow
    console.log(findFollower, findUser);
    if (!findFollower || !findUser) {
      return {
        result: "failed",
        status: STATUS.NOT_FOUND,
      };
    }

    const arrFling = findFollower.followings; // mảng những người đang following
    const arrFler = findUser.followers; // mảng những người đang được follow

    const indexFling = arrFling.findIndex((user) => user.id == userID);
    const indexFler = arrFler.findIndex((user) => user.id == followerID);
    if (indexFling !== -1 & indexFler !== -1) {
      arrFling.splice(indexFling, 1);
      arrFler.splice(indexFler, 1);

      await findFollower.updateOne({
        followings: arrFling,
      });
      await findUser.updateOne({
        followers: arrFler,
      });

      return {
        result: "success",
        status: STATUS.OK,
      };
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetFollowerService = async (id) => {
  try {
    const user = await User.findById(id);
    if (user) {
      return {
        result: "success",
        status: STATUS.OK,
        followers: user.followers,
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetFollowingService = async (id) => {
  try {
    const user = await User.findById(id);
    if (user) {
      return {
        result: "success",
        status: STATUS.OK,
        followers: user.followings,
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetFollowingService1 = async (id) => {
  try {
    const user = await User.findById(id);
    if (user) {
      return {
        result: "success",
        status: STATUS.OK,
        followers: user.followings,
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy người dùng",
    };
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

module.exports = {
  RegisterUserService,
  LoginUserService,
  LogoutService,
  ChangePasswordService,
  GetProfileUserService,
  GetListUserService,
  UpdateProfileService,
  UpdateAvatarService,
  LockUserService,
  UnLockUserService,
  SearchUserService,
  FollowService,
  UnFollowService,
  GetFollowerService,
  GetFollowingService,
  GetFollowingService1
};
