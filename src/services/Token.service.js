const { TOKENTYPE, STATUS, CONFIG } = require("../configs/config");
const { signToken } = require("../middlewares/jwt");
const User = require("../models/User.model");

const createToken = async ({ id, type, role, status, expire }) => {
  try {
    const token = await signToken({ id, type, role, status }, expire);
    return token;
  } catch (error) {
    console.log(error);
  }
};

const updateToken = async ({ id, role, status }) => {
  try {
    const accessToken$ = await signToken({ id, type: TOKENTYPE.accessToken, role, status }, CONFIG.jwt_expire_access_token);
    const accessRefresh$ = await signToken({ id, type: TOKENTYPE.accessToken, role, status }, CONFIG.jwt_expire_refresh_token);
    const accessToken = await Promise.all([token]);
    const Token = await db.Token.update(
      {
        where: {
          [Op.and]: [{ accountId: accountId }, { type: type }],
        },
      },
      {
        type: type,
        accountId: accountId,
        token: accessToken.toString(),
      }
    );
    return Token.token;
  } catch (error) {
    console.log(error);
  }
};

module.exports = { createToken, updateToken };
