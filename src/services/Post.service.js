const { SERVER_ERROR, SERVER_DB_ERROR, STATUS } = require("../configs/config");
const { STATUS_POST } = require("../configs/constant");
const Post = require("../models/Post.model");
const User = require("../models/User.model");
const Report = require("../models/Report.model");
const {
  uploadToCloudinary,
  removeFolderFromCloudinary,
} = require("./cloudinary");

const CreatePostService = async (data, userId, files) => {
  try {
    const post = new Post({
      userId: userId,
      content: data?.content,
      status: data?.status,
    });

    if (post && files?.image?.length > 0) {
      const arrImg = [];
      for (let file of files?.image) {
        const img = await uploadToCloudinary(
          file.path,
          `be-web/${userId}/${post._id}`
        );
        arrImg.push(img.url);
      }
      post.set({
        images: arrImg,
      });
    }

    await post.save();

    return {
      result: "success",
      status: 202,
      post: post,
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const DeletePostService = async (userId, role, id) => {
  try {
    const post = await Post.findById(id);

    if (post) {
      if (role === 0 || (role === 1 && post.userId == userId)) {
        // if (post.images.length !== 0) {
        //   await removeFolderFromCloudinary(`be-web/${post.userId}/${post._id}`);
        // }

        const reportPost = await Report.find({
          reportId: post._id,
        });

        //console.log(reportPost);

        reportPost.forEach(async (report) => {
          await Report.deleteOne({ reportId: report.reportId });
        });

        if (post.share.length !== 0) {
          post.share.map(async (id) => await Post.findByIdAndDelete(id));
        }

        await post.deleteOne();
        return {
          result: "success",
          status: 200,
        };
      }
      return {
        result: "failed",
        status: STATUS.FORBIDDEN,
        reason: "Không được cấp quyền này",
      };
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy bài viết ",
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const EditPostService = async (userId, id, data) => {
  try {
    const post = await Post.findOne({
      $and: [
        {
          _id: id,
        },
        {
          userId: userId,
        },
      ],
    });

    if (post) {
      post.set({
        content: data?.content,
      });

      await post.save();
      return {
        result: "success",
        status: 200,
        post: post,
      };
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy bài viết ",
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const ChangeLikePostService = async (userId, postId) => {
  try {
    const post = await Post.findById(postId);
    if (post) {
      const like = post.likes.find((user) => user === userId);
      if (like) {
        const index = post.likes.indexOf(userId);
        post.likes.splice(index, 1);
        await post.save();
      } else {
        post.likes.push(userId);
        await post.save();
      }
      return {
        result: "success",
        status: 200,
        like: post.likes,
      };
    }

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy bài viết ",
    };
  } catch (error) {
    //console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetPostService = async (id) => {
  try {
    const post = await Post.findById(id);
    if (post)
      return {
        result: "success",
        status: STATUS.OK,
        post: post,
      };

    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy bài viết ",
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const CreateCommentService = async ({ postId, userId, comment }) => {
  try {
    const post = await Post.findById(postId);
    const user = await User.findById(userId);
    if (!user)
      return {
        result: "failed",
        status: STATUS.NOT_FOUND,
        reason: "Không tìm thấy người dùng ",
      };
    if (post) {
      let commentList = post.comments;
      commentList.push({
        user: userId,
        comment: comment,
        name: user?.fullname,
        avatar: user?.avatar,
        date: Date.now(),
        isEdit: false,
      });

      await post.set({
        comments: commentList,
      });

      await post.save();

      return {
        result: "success",
        status: STATUS.CREATE_OK,
        post: post,
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy bài viết ",
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const EditCommentService = async ({ postId, userId, comment, date }) => {
  try {
    const post = await Post.findById(postId);
    if (post) {
      let commentList = post.comments;
      const index = commentList.findIndex((cmt) => cmt.date == date);
      if (index === -1)
        return {
          result: "failed",
          status: STATUS.BAD_REQUEST,
          reason: "Không tìm thất bình luận này",
        };

      commentList.splice(index, 1);
      commentList.push({
        user: userId,
        comment: comment,
        date: Date.now(),
        isEdit: true,
      });
      await post.save();

      return {
        result: "success",
        status: STATUS.CREATE_OK,
        post: post,
      };
    }
    return {
      result: "failed",
      status: STATUS.NOT_FOUND,
      reason: "Không tìm thấy bài viết ",
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const SharePostService = async (userId, postId, data) => {
  try {
    const findPost = await Post.findById(postId);
    const findUser = await User.findById(userId);
    if (!findPost || !findUser) {
      return {
        result: "success",
        status: STATUS.NOT_FOUND,
        reason: "Không tìm thấy người bài viết",
      };
    }
    let post;
    if (findPost.sharePostId) {
      post = new Post({
        userId: userId,
        sharePostId: findPost.sharePostId,
        content: data?.content,
        status: data?.status,
      });
    } else {
      post = new Post({
        userId: userId,
        sharePostId: postId,
        content: data?.content,
        status: data?.status,
      });
    }

    await post.save();

    findPost.share.push(post._id);
    await findPost.save();

    return {
      result: "success",
      status: STATUS.OK,
      post: post,
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetAllPostOfUserService = async (id) => {
  try {
    const user = await User.findById(id);
    if (!user)
      return {
        result: "failed",
        reason: "Không tìm thấy người dùng",
        status: STATUS.NOT_FOUND,
      };
    const listPost = await Post.find({ userId: id }).sort({ createdAt: -1 });
    console.log("123");
    return {
      result: "success",
      status: STATUS.OK,
      post: listPost,
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetAllPostOfOtherUserService = async (id, userId, role) => {
  try {
    const user = await User.findById(id);
    const otherUser = await User.findById(userId);
    if (!otherUser)
      return {
        result: "failed",
        reason: "Không tìm thấy người dùng",
        status: STATUS.NOT_FOUND,
      };

    console.log("123");

    // if (role === 0) {
    //   console.log('find');
    //   const listPost = await Post.find({
    //     userId: userId,
    //   }).sort({ createdAt: -1 });
    //   return {
    //     result: "success",
    //     status: STATUS.OK,
    //     post: listPost,
    //   };
    // }

    if (user.followings.includes(userId) && otherUser.followers.includes(id)) {
      const listPost = await Post.find({
        $and: [
          {
            userId: userId,
          },
          {
            $or: [
              { status: STATUS_POST.PUBLIC },
              {
                status: STATUS_POST.FOLLOWER,
              },
            ],
          },
        ],
      }).sort({ createdAt: -1 });
      return {
        result: "success",
        status: STATUS.OK,
        post: listPost,
      };
    }

    const listPost = await Post.find({
      $and: [
        {
          userId: userId,
        },
        {
          status: STATUS_POST.PUBLIC,
        },
      ],
    }).sort({ createdAt: -1 });
    return {
      result: "success",
      status: STATUS.OK,
      post: listPost,
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

const GetAllPostService = async () => {
  try {
    const post = await Post.find({});
    return {
      result: "success",
      post: post,
      status: STATUS.OK,
    };
  } catch (error) {
   // console.log(error);
    return SERVER_ERROR(error);
  }
};

module.exports = {
  CreatePostService,
  DeletePostService,
  EditPostService,
  ChangeLikePostService,
  GetPostService,
  CreateCommentService,
  EditCommentService,
  SharePostService,
  GetAllPostOfUserService,
  GetAllPostOfOtherUserService,
  GetAllPostService,
};
