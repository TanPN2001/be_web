const { STATUS_POST } = require("../../configs/constant");

module.exports = {
  async up(db, client) {
    // TODO write your migration here.
    // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
    // Example:
    await db.collection("posts").create({
      userId: "64916f54a53c6a64c1d9641d",
      status: STATUS_POST.PUBLIC,
      content: "Post migration",
    });
  },

  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    await db.collection("posts").create({
      userId: "64916f54a53c6a64c1d9641d",
      status: STATUS_POST.PUBLIC,
      content: "Post migration",
    });
  },
};
