const { SERVER_ERROR, STATUS } = require("../configs/config");

const isAdminOfGroup = async (req, res, next) => {
    try {
        
        return next()
      } catch (error) {
        console.log(error);
        res.send(SERVER_ERROR(error));
      }

}

module.exports = {isAdminOfGroup}