const { SERVER_ERROR, CONFIG, STATUS } = require("../configs/config");
const User = require("../models/User.model");
const jwt = require("./jwt");

const isAuthenticated = async (req, res, next) => {
  try {
    if (req.headers["authorization"]) {
      const token = req.headers["authorization"].split(" ")[1];
      const decodeToken = await jwt.verifyToken(token);
      const user = await User.findOne({
        _id: decodeToken.id,
      });

      if (user.status !== 1)
        return res.send({
          result: "failed",
          status: STATUS.NOT_ACCEPTABLE,
          reason: "Tài khoản đã bị khóa",
        });

      if (!user.accessToken)
        return res.send({
          result: "failed",
          status: STATUS.UNAUTHORIZED,
          reason: "Bạn cần đăng nhập!",
        });

      if (user) {
        req.USER_ID = decodeToken.id;
        req.ROLE = decodeToken.role;
        return await next();
      }
    }

    return res.send({
      result: "Unauthorized",
      status: STATUS.UNAUTHORIZED,
      reason: "Bạn cần đăng nhập!",
    });
  } catch (error) {
    console.log(error);
    res.send(SERVER_ERROR(error));
  }
};

const isAdmin = async (req, res, next) => {
  try {
    if (req.headers["authorization"]) {
      const token = req.headers["authorization"].split(" ")[1];
      const decodeToken = await jwt.verifyToken(token);
      const user = await User.findOne({
        $and: [
          { _id: decodeToken.id },
          {
            role: 0,
          },
        ],
      });

      if (user) {
        req.USER_ID = decodeToken.id;
        req.ROLE = decodeToken.role;
        return await next();
      }
    }

    return res.send({
      result: "Unauthorized",
      status: STATUS.UNAUTHORIZED,
      reason: "Bạn cần đăng nhập!",
    });
  } catch (error) {
    res.send(SERVER_ERROR(error));
  }
};

const isUser = async (req, res, next) => {
  try {
    if (req.headers["authorization"]) {
      const token = req.headers["authorization"].split(" ")[1];
      const decodeToken = await jwt.verifyToken(token);
      const user = await User.findOne({
        $and: [
          { _id: decodeToken.id },
          {
            role: 1,
          },
        ],
      });

      if (user) {
        req.USER_ID = decodeToken.id;
        req.ROLE = decodeToken.role;
        return await next();
      }
    }

    return res.send({
      result: "Unauthorized",
      status: STATUS.UNAUTHORIZED,
      reason: "Bạn cần đăng nhập!",
    });
  } catch (error) {
    res.send(SERVER_ERROR(error));
  }
};

module.exports = { isAuthenticated, isAdmin, isUser };
