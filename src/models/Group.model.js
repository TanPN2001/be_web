const mongoose = require("mongoose");
const GroupShema = new mongoose.Schema({
  admin: {
    type: mongoose.Types.ObjectId,
  },
  mod: {
    type: Array,
  },
  name: {
    type: String,
  },
  member: {
    type: Array,
  },
  memberRequest: {
    type: Array
  },
  cover: {
    type: String,
  },
  description: {
    type: String,
  },
  post: {
    type: Array,
  },
  status: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

const Group = mongoose.model("Group", GroupShema);
module.exports = Group;
