

const mongoose = require("mongoose");
const UserShema = new mongoose.Schema({
  role: {
    type: Number,
  },
  email: {
    type: String,
    unique: true,
    sparse: true,
  },
  password: {
    type: String,
  },
  fullname: {
    type: String,
  },
  phone: {
    type: String,
    unique: true,
    sparse: true,
  },
  dob: {
    type: Date,
  },
  username: {
    type: String,
    unique: true,
    sparse: true,
  },
  followings: {
    type: Array,
  },
  followers: {
    type: Array,
  },
  avatar: {
    type: String,
    default: 'https://res.cloudinary.com/duzvm6old/image/upload/v1681894739/be-web/public/default-avatar.png'
  },
  status: {
    type: Number,
  },
  accessToken: {
    type: String,
  },
  refreshToken: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  },
});

const User = mongoose.model("User", UserShema);
module.exports = User;
