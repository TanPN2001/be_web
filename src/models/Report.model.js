const mongoose = require("mongoose");
const ReportSchema = new mongoose.Schema({
  fromUserId: { type: mongoose.Schema.ObjectId, required: true },
  fromUserEmail: { type: String },
  toUserId: { type: mongoose.Schema.ObjectId, required: true },
  toUserEmail: { type: String },
  reportId: { type: mongoose.Schema.ObjectId, required: true },
  type: { type: String, enum: ["POST", "USER"] },
  description: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

const Report = mongoose.model("Report", ReportSchema);
module.exports = Report;
