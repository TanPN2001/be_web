const mongoose = require("mongoose");
const { STATUS_POST } = require("../configs/constant");
const PostShema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.ObjectId,
    require: true,
  },
  sharePostId: {
    type: mongoose.Schema.ObjectId,
    default: null
  },
  status: {
    type: String,
    default: STATUS_POST.PUBLIC,
    enum: [STATUS_POST.PUBLIC, STATUS_POST.PRIVATE, STATUS_POST.FOLLOWER]
  },
  content: {
    type: String,
  },
  images: {
    type: Array,
  },
  likes: {
    type: Array,
  },
  share: {
    type: Array,
  },
  comments: {
    type: Array,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

const Post = mongoose.model("Post", PostShema);
module.exports = Post;
