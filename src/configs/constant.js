// post
module.exports.STATUS_POST = {
    PUBLIC: 'public',
    PRIVATE: 'private',
    FOLLOWER: 'follower'
}

module.exports.DEFAULT_USER = {
    AVATAR: "https://res.cloudinary.com/duzvm6old/image/upload/v1687845149/be-web/public/default-avatar.webp"
}

module.exports.NUM_SUGGESTION = 5