const dotenv = require("dotenv");
dotenv.config();

const STATUS = {
  OK: 200,
  CREATE_OK: 201,
  CONTINUE: 100,
  GATEWAY_TIMEOUT: 504,
  REQUEST_TIMEOUT: 408,
  UNAUTHORIZED: 401,  // need login
  UNPROCESSABLE_ENTITY: 422,
  UNSUPPORTED_MEDIA_TYPE: 415,
  TOO_MANY_REQUESTS: 429,
  INTERNAL_SERVER_ERROR: 500,
  BAD_GATEWAY: 502,
  BAD_REQUEST: 400,  // error input
  FORBIDDEN: 403,  // dont permission
  NOT_FOUND: 404,  // not found
  NOT_ACCEPTABLE: 406,  // lock account
};

const GROUP_STATUS = {
  PRIVATE: "private",
  PUBLIC: "public"
}

const TOKENTYPE = {
  accessToken: "ACCESS_TOKEN",
  refreshToken: "REFRESH_TOKEN",
};

const CONFIG = {
  port: process.env.PORT,
  secret: process.env.SECRET,
  bcrypt_salt: process.env.SALT,
  jwt_expire_access_token: '24h',
  jwt_expire_refresh_token: "24h",
};

const SERVER_DB_ERROR = {
  result: "failed",
  reason: "Lỗi máy chủ cơ sở dữ liệu",
  status: 502
};

const SERVER_ERROR = (error) => ({
  result: "Lỗi máy chủ",
  error: error,
  status: 504
});

const NOT_FOUND_RESULT = {
  result: 'failed',
  reason: 'Lỗi máy chủ',
  status: 503
}

const LACK_OF_PARAMS = {
  result: "failed",
  reason: "Thiếu dữ liệu đầu vào",
  status: 400
};

module.exports = {
  STATUS,
  TOKENTYPE,
  CONFIG,
  SERVER_DB_ERROR,
  LACK_OF_PARAMS,
  SERVER_ERROR,
  NOT_FOUND_RESULT,
  GROUP_STATUS
};
