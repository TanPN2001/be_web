const mongoose = require("mongoose");
const url =
  `mongodb+srv://${process.env.USER}:${process.env.PASS}@cluster0.vrkohsj.mongodb.net/BE-WEB?retryWrites=true&w=majority`;
async function connectDB() {
  try {
    await mongoose
      .connect(url)
      .then(() => console.log("Connected to DB!"))
      .catch((err) => console.log(err));
  } catch (error) {
    console.log(error);
  }
}

module.exports = connectDB
