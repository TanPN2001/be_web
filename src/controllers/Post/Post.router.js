const express = require("express");
const { isAuthenticated, isAdmin, isUser } = require("../../middlewares/auth");
const PostRouter = express.Router();
const PostController = require("./Post.controller");
const uploadFile = require("../../middlewares/upload");

PostRouter.post(
  "/create",
  uploadFile.fields([{ name: "image" }]),
  isAuthenticated,
  PostController.CreatePostController
);
PostRouter.put('/comment', isAuthenticated, PostController.EditCommentController)
PostRouter.post('/like/:id', isAuthenticated, PostController.ChangeLikePostController)
PostRouter.post('/comment', isAuthenticated, PostController.CreateCommentController)
PostRouter.post('/share/:id', isAuthenticated, PostController.SharePostController)
PostRouter.get('/user', isAuthenticated, PostController.GetAllPostOfUserController)
PostRouter.get('/all-post/:id', isAuthenticated, PostController.GetAllPostOfOtherUserController)
PostRouter.get("/analytics",isAdmin, PostController.GetAllPostController);
PostRouter.put("/:id", isAuthenticated, PostController.EditPostController);
PostRouter.delete("/:id", isAuthenticated, PostController.DeletePostController);
PostRouter.get("/:id", isAuthenticated, PostController.GetPostController);

module.exports = PostRouter;
