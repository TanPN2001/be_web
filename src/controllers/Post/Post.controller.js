const {
  SERVER_ERROR,
  NOT_FOUND_RESULT,
  LACK_OF_PARAMS,
} = require("../../configs/config");
const {
  CreatePostService,
  DeletePostService,
  EditPostService,
  ChangeLikePostService,
  GetPostService,
  CreateCommentService,
  EditCommentService,
  SharePostService,
  GetAllPostOfUserService,
  GetAllPostOfOtherUserService,
  GetAllPostService,
} = require("../../services/Post.service");
const { GetFollowerService, GetFollowingService } = require("../../services/User.service");

const CreatePostController = async (req, res) => {
  try {
    if (!req.USER_ID) return res.send(LACK_OF_PARAMS);
    const result = await CreatePostService(req.body, req.USER_ID, req.files);

    if (result) return res.send(result);

    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const GetPostController = async (req, res) => {
  try {
    const { id } = req.params;
    if (!id) return res.send(LACK_OF_PARAMS);
    const result = await GetPostService(id);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const DeletePostController = async (req, res) => {
  try {
    const { id } = req.params;
    if (!req.USER_ID || !id) return res.send(LACK_OF_PARAMS);
    const result = await DeletePostService(req.USER_ID, req.ROLE, id);
    if (result) return res.send(result);

    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const EditPostController = async (req, res) => {
  try {
    const { id } = req.params;
    if (!id || !req.USER_ID) return res.send(LACK_OF_PARAMS);

    const result = await EditPostService(req.USER_ID, id, req.body);

    if (result) return res.send(result);

    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const ChangeLikePostController = async (req, res) => {
  try {
    const { id } = req.params;
    if (!id || !req.USER_ID) return res.send(LACK_OF_PARAMS);
    const result = await ChangeLikePostService(req.USER_ID, id);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return SERVER_ERROR(error);
  }
};

const CreateCommentController = async (req, res) => {
  try {
    const { postId, comment } = req.body;
    const userId = req.USER_ID;
    if (!postId || !userId || !comment) return res.send(LACK_OF_PARAMS);
    const result = await CreateCommentService({ postId, userId, comment });
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const EditCommentController = async (req, res) => {
  try {
    const { postId, comment, date } = req.body;
    const userId = req.USER_ID;
    if (!postId || !userId || !comment || !date)
      return res.send(LACK_OF_PARAMS);
    const result = await EditCommentService({ postId, userId, comment, date });
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const SharePostController = async (req, res) => {
  try {
    const { id } = req.params;
    if (!id || !req.USER_ID) return res.send(LACK_OF_PARAMS);
    const result = await SharePostService(req.USER_ID, id, req.body);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const GetAllPostOfUserController = async (req, res) => {
  try {
    const id = req.USER_ID;
    if (!id) return res.send(LACK_OF_PARAMS);
    const result = await GetAllPostOfUserService(id);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const GetAllPostOfOtherUserController = async (req, res) => {
  try {
    const {id} = req.params;
    if (!id || !req.USER_ID) return res.send(LACK_OF_PARAMS);

    const result = await GetAllPostOfOtherUserService(req.USER_ID,id)
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const GetAllPostController = async (req, res) => {
  try {
    const result = await GetAllPostService()
    return res.send(result)
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
}



module.exports = {
  CreatePostController,
  GetPostController,
  DeletePostController,
  EditPostController,
  ChangeLikePostController,
  CreateCommentController,
  EditCommentController,
  SharePostController,
  GetAllPostOfUserController,
  GetAllPostOfOtherUserController,
  GetAllPostController
};
