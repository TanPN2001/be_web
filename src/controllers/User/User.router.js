const express = require("express");
const { isAuthenticated, isAdmin } = require("../../middlewares/auth");
const UserRouter = express.Router();
const UserController = require("./User.controller");
const uploadFile = require("../../middlewares/upload");

UserRouter.post("/register", UserController.RegisterUserController);
UserRouter.post("/login", UserController.LoginUserController);
UserRouter.put("/logout",isAuthenticated ,UserController.LogoutController);
UserRouter.get(
  "/profile",
  isAuthenticated,
  UserController.GetProfileUserController
);
UserRouter.put(
  "/profile",
  isAuthenticated,
  UserController.UpdateProfileController
);
UserRouter.put('/change-password', isAuthenticated, UserController.ChangePasswordController)
UserRouter.put('/upload-avatar', uploadFile.single('avatar') ,isAuthenticated, UserController.UpdateAvatarController)
UserRouter.put("/lock", isAdmin, UserController.LockUserController);
UserRouter.put("/unlock", isAdmin, UserController.UnLockUserController);
UserRouter.get("/search", isAuthenticated, UserController.SearchUserController);
UserRouter.get("/list-user", isAuthenticated, UserController.GetListUserController);
UserRouter.put("/follow/:id", isAuthenticated, UserController.FollowController)
UserRouter.put("/unfollow/:id", isAuthenticated, UserController.UnFollowController)
UserRouter.get('/follower/:id', isAuthenticated, UserController.GetFollowerController)
UserRouter.get('/following/:id', isAuthenticated, UserController.GetFollowingController)
UserRouter.get("/:id",isAuthenticated, UserController.GetUserByIDController);

module.exports = UserRouter;
