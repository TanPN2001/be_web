const {
  SERVER_ERROR,
  LACK_OF_PARAMS,
  NOT_FOUND_RESULT,
} = require("../../configs/config");
const {
  RegisterUserService,
  LoginUserService,
  GetProfileUserService,
  GetListUserService,
  UpdateProfileService,
  LockUserService,
  UnLockUserService,
  SearchUserService,
  UpdateAvatarService,
  LogoutService,
  ChangePasswordService,
  FollowService,
  UnFollowService,
  GetFollowingService,
  GetFollowerService,
} = require("../../services/User.service");

const RegisterUserController = async (req, res) => {
  try {
    if (!req.body.password || !req.body.email || !req.body.fullname) {
      return res.send(LACK_OF_PARAMS);
    }
    const result = await RegisterUserService(req.body);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const LoginUserController = async (req, res) => {
  try {
    if (!req.body.email || !req.body.password) return res.send(LACK_OF_PARAMS);
    const result = await LoginUserService(req.body);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const LogoutController = async (req, res) => {
  try {
    const userId = req.USER_ID;
    if (!userId) return res.send(LACK_OF_PARAMS);
    const result = await LogoutService(userId);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const ChangePasswordController = async (req, res) => {
  try {
    const { oldPassword, newPassword } = req.body;
    const userId = req.USER_ID;
    if (!oldPassword || !newPassword || !userId)
      return res.send(LACK_OF_PARAMS);
    const result = await ChangePasswordService({
      oldPassword,
      newPassword,
      userId,
    });
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const GetProfileUserController = async (req, res) => {
  try {
    if (!req.USER_ID)
      return res.send(LACK_OF_PARAMS());
    const result = await GetProfileUserService(req.USER_ID);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const GetUserByIDController = async (req, res) => {
  try {
    if (!req.params.id) return res.send(LACK_OF_PARAMS);

    const result = await GetProfileUserService(req.params.id);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const GetListUserController = async (req, res) => {
  try {
    const result = await GetListUserService(req.params.id);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const UpdateProfileController = async (req, res) => {
  try {
    const id = req.USER_ID;
    if (!id) return res.send(LACK_OF_PARAMS);
    const result = await UpdateProfileService(req.body, id);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const UpdateAvatarController = async (req, res) => {
  try {
    if (!req.file || !req.USER_ID)
      return res.send(LACK_OF_PARAMS());

    const result = await UpdateAvatarService(req.file, req.USER_ID);

    if (result) return res.send(result);
    return res.send(SERVER_ERROR());
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const LockUserController = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id || !req.USER_ID) return res.send(LACK_OF_PARAMS);

    if (req.ROLE !== 0)
      return res.send({
        result: "failed",
        reason: `Không được cấp quyền!`,
      });
    const result = await LockUserService(req.USER_ID, id);

    if (result) return res.send(result);

    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const UnLockUserController = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id || !req.USER_ID) return res.send(LACK_OF_PARAMS);

    if (req.ROLE !== 0)
      return res.send({
        result: "failed",
        reason: `Không được cấp quyền!`,
      });
    const result = await UnLockUserService(req.USER_ID, id);

    if (result) return res.send(result);

    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const SearchUserController = async (req, res) => {
  try {
    const { q, limit, page } = req.query;
    const result = await SearchUserService({ q, limit, page });
    if (result) {
      return res.send(result);
    }

    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const CreateUserController = async (req, res) => {
  try {
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
};

const FollowController = async (req, res) => {
  try {
    const {id} = req.params
    if (!id || !req.USER_ID) return res.send(LACK_OF_PARAMS);
    const result = await FollowService(req.USER_ID, id);
    if (result) {
      return res.send(result);
    }
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
}

const UnFollowController = async (req, res) => {
  try {
    const {id} = req.params
    if (!id || !req.USER_ID) return res.send(LACK_OF_PARAMS);
    const result = await UnFollowService(req.USER_ID, id);
    if (result) {
      return res.send(result);
    }
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    return res.send(SERVER_ERROR(error));
  }
}

const GetFollowerController = async (req, res) => {
  try {
    const {id} = req.params;
    if (!id) return res.send(LACK_OF_PARAMS);
    const result = await GetFollowerService(id);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const GetFollowingController = async (req, res) => {
  try {
    const {id} = req.params;
    if (!id) return res.send(LACK_OF_PARAMS);
    const result = await GetFollowingService(id);
    if (result) return res.send(result);
    return res.send(NOT_FOUND_RESULT);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};


module.exports = {
  RegisterUserController,
  LoginUserController,
  LogoutController,
  ChangePasswordController,
  GetProfileUserController,
  GetUserByIDController,
  GetListUserController,
  UpdateProfileController,
  LockUserController,
  UnLockUserController,
  SearchUserController,
  UpdateAvatarController,
  FollowController,
  UnFollowController,
  GetFollowerController,
  GetFollowingController
};
