const express = require("express");
const { isAuthenticated } = require("../../middlewares/auth");
const { GetPublicNewFeedController, GetSuggestionController } = require("./NewFeed.controller");
const NewFeedRouter = express.Router()

NewFeedRouter.get('/suggestion', isAuthenticated, GetSuggestionController)
NewFeedRouter.get('/', isAuthenticated, GetPublicNewFeedController)

module.exports = NewFeedRouter