const { SERVER_ERROR } = require("../../configs/config");
const {
  GetPublicNewFeedService,
  GetSuggestionService,
} = require("../../services/NewFeed.service");

const GetPublicNewFeedController = async (req, res) => {
  try {
    if (!req.USER_ID) return res.send(LACK_OF_PARAMS);
    const result = await GetPublicNewFeedService(req.USER_ID);
    if (result) return res.send(result);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

const GetSuggestionController = async (req, res) => {
  try {
    if (!req.USER_ID) return res.send(LACK_OF_PARAMS);
    const result = await GetSuggestionService(req.USER_ID);
    if (result) return res.send(result);
  } catch (error) {
    console.log(error);
    return res.send(SERVER_ERROR(error));
  }
};

module.exports = {
  GetPublicNewFeedController,
  GetSuggestionController,
};
