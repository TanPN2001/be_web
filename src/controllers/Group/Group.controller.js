const { SERVER_ERROR, NOT_FOUND_RESULT, LACK_OF_PARAMS } = require("../../configs/config")
const { CreateGroupService, DeleteGroupService, ChangeStatusGroupService, RenameGroupService, JoinToGroupService, LeaveFromGroupService, SearchGroupService } = require("../../services/Group.service")

const CreateGroupController = async (req, res) => {
    try {
        const id = req.USER_ID
        if(!id || !req.body.name || !req.body.status) return res.send(LACK_OF_PARAMS)
        const result = await CreateGroupService(id, req.body)
        if(result) return res.send(result);
        return res.send(NOT_FOUND_RESULT);
    } catch (error) {
        console.log(error);
        return res.send(SERVER_ERROR(error))
    }
}

const DeleteGroupController = async (req, res) => {
    try {
        const role = req.ROLE
        const userId = req.USER_ID
        const {id} = req.params
        if(!userId || !role || !id) return res.send(LACK_OF_PARAMS)
        const result = await DeleteGroupService({groupId: id, userId, role})
        if(result) return res.send(result)
        return res.send(NOT_FOUND_RESULT)
    } catch (error) {
        console.log(error);
        return res.send(SERVER_ERROR(error))
    }
}

const ChangeStatusGroupController = async (req, res) => {
    try {
        const role = req.ROLE
        const userId = req.USER_ID
        const {status, groupId} = req.body
        if(!userId || !role || !groupId || !status) return res.send(LACK_OF_PARAMS)
        const result = await ChangeStatusGroupService({groupId, userId, role, status})
        if(result) return res.send(result)
        return res.send(NOT_FOUND_RESULT)
    } catch (error) {
        console.log(error);
        return res.send(SERVER_ERROR(error))
    }
}

const RemoveMemberFromGroupController = async (req,  res)=>{
    try {
        
    } catch (error) {
        console.log(error);
        return res.send(SERVER_ERROR(error))
    }
}

const RenameGroupController = async (req,  res)=>{
    try {
        const role = req.ROLE
        const userId = req.USER_ID
        const {name, groupId} = req.body
        if(!userId || !role || !groupId || !name) return res.send(LACK_OF_PARAMS)
        const result = await RenameGroupService({ groupId, userId, role, name })
        if(result) return res.send(result)
        return res.send(NOT_FOUND_RESULT)
    } catch (error) {
        console.log(error);
        return res.send(SERVER_ERROR(error))
    }
}

const JoinToGroupController = async (req, res) => {
    try {
        const memberId = req.USER_ID
        const {id} = req.params
        if(!memberId || !id) return res.send(LACK_OF_PARAMS)
        const result = await JoinToGroupService({groupId: id, memberId})
        if(result) return res.send(result)
        return res.send(NOT_FOUND_RESULT)
    } catch (error) {
        console.log(error);
        return res.send(SERVER_ERROR(error))
    }
}

const LeaveFromGroupController = async (req, res) => {
    try {
        const userId = req.USER_ID
        const {id} = req.params
        if(!userId || !id) return res.send(LACK_OF_PARAMS)
        const result = await LeaveFromGroupService({groupId: id, userId})
        if(result) return res.send(result)
        return res.send(NOT_FOUND_RESULT)
    } catch (error) {
        console.log(error);
        return res.send(SERVER_ERROR(error))
    }
}

const SearchGroupController = async (req, res) => {
    try {
        const {limit, page, q} = req.query
        const result = await SearchGroupService({limit, page, q})
        if(result) return res.send(result)
        return res.send(NOT_FOUND_RESULT)
    } catch (error) {
        console.log(error);
        return res.send(SERVER_ERROR(error))
    }
}

module.exports = {CreateGroupController, DeleteGroupController, ChangeStatusGroupController, RemoveMemberFromGroupController, RenameGroupController, JoinToGroupController, LeaveFromGroupController, SearchGroupController}