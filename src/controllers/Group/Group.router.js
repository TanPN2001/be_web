const express = require("express");
const { CreateGroupController, DeleteGroupController, ChangeStatusGroupController, RenameGroupController, JoinToGroupController, LeaveFromGroupController, SearchGroupController } = require("./Group.controller");
const { isAuthenticated } = require("../../middlewares/auth");
const GroupRouter = express.Router()

GroupRouter.post("/create", isAuthenticated ,CreateGroupController)
GroupRouter.get("/" ,SearchGroupController)
GroupRouter.delete("/:id", isAuthenticated ,DeleteGroupController)
GroupRouter.put('/change-status', isAuthenticated, ChangeStatusGroupController)
GroupRouter.put('/rename-group', isAuthenticated, RenameGroupController)
GroupRouter.put('/join-group/:id', isAuthenticated, JoinToGroupController)
GroupRouter.put('/leave-group/:id', isAuthenticated, LeaveFromGroupController)

module.exports = GroupRouter