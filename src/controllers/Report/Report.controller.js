const { Mongoose, default: mongoose } = require("mongoose");
const { NOT_FOUND_RESULT, LACK_OF_PARAMS } = require("../../configs/config");
const Post = require("../../models/Post.model");
const Report = require("../../models/Report.model");
const User = require("../../models/User.model");

module.exports = {
  async CreateReportController(req, res) {
    try {
      const { reportId, description, type } = req.body;
      const userId = req?.USER_ID;
      if (!["POST", "USER"].includes(type))
        return res.send({
          result: "failed",
          reason: "Lỗi định dạng đầu vào",
        });

      if (!reportId || !userId || !description || !type)
        return res.send(LACK_OF_PARAMS);
      let toUser, post;

      const id = new mongoose.Types.ObjectId(reportId);
      if (type === "POST") {
        post = await Post.findById(id);
        if (!post)
          return res.send({
            result: "failed",
            reason: "Không tìm thấy bài viết",
          });
      }
      if (type === "USER") {
        console.log("u");
        toUser = await User.findById(id);
        if (!toUser)
          return res.send({
            result: "failed",
            reason: "Không tìm thấy người dùng",
          });
      }

      let fromUser = await User.findById(userId);
      if (!fromUser)
        return res.send({
          result: "failed",
          reason: "Không tìm thấy người dùng",
        });

      const report = new Report({
        reportId: id,
        fromUserId: fromUser._id,
        fromUserEmail: fromUser?.email,
        toUserId: type === "POST" ? post.userId : toUser._id,
        toUserEmail: type === "USER" ? toUser.email : "",
        description,
        type,
      });

      await report.save();
      return res.send({
        result: "success",
        report: report,
      });
    } catch (error) {
      return res.send({ result: "failed", error: error?.message });
    }
  },
  async GetAllReportController(req, res) {
    try {
      const report = await Report.find({});
      return res.send({
        result: "success",
        report: report,
      });
    } catch (error) {
      return res.send({
        result: "failed",
        error: error?.message,
      });
    }
  },
  async DeleteReportController(req, res) {
    try {
      const { id } = req.params;
      if (!id) return res.send(LACK_OF_PARAMS);
      const report = await Report.findByIdAndDelete(id);
      if (!report)
        return res.send({
          result: "failed",
          reason: "Không tìm thấy báo cáo",
        });

      return res.send({
        result: "success",
      });
    } catch (error) {
      return res.send({
        result: "failed",
        error: error?.message,
      });
    }
  },
};
