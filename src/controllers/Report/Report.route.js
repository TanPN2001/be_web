const express = require("express");
const { isAuthenticated, isAdmin, isUser } = require("../../middlewares/auth");
const ReportRoute = express.Router();
const uploadFile = require("../../middlewares/upload");
const ReportController = require("./Report.controller");

ReportRoute.post('/create', isAuthenticated, ReportController.CreateReportController)
ReportRoute.delete('/:id', ReportController.DeleteReportController)
ReportRoute.get('', isAdmin, ReportController.GetAllReportController)


module.exports = ReportRoute;
