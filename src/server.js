const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

// const corsOptions = {
//   origin: 'http://localhost:7777',
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// }

const config = require("./configs/config");

const app = express();
app.use(cors({}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const connectDB = require("./configs/connectDB");
connectDB();

app.get("/", function (req, res) {
  res.send("Hello World");
});

const user = require("./controllers/User/User.router");
app.use("/user", user);

const post = require("./controllers/Post/Post.router");
app.use("/post", post);

const group = require("./controllers/Group/Group.router");
app.use("/group", group);

const new_feed = require("./controllers/NewFeed/NewFeed.router");
app.use("/new-feed", new_feed);

const report = require('./controllers/Report/Report.route')
app.use('/report', report)

app.listen(config.CONFIG.port, () => {
  console.log("Server started on", config.CONFIG.port);
});
